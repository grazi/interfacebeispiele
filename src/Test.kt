fun main(args: Array<String>) {

    val konten = arrayListOf<IKonto>(
            SparKonto("Graziano", 1, 0.0),
            LohnKonto("Fabio", 2, 100.0),
            DummyKonto()
    )


    zahleAlleKontos150CHF(konten)
    gibAlleKontoStaendeAus(konten)


}

fun zahleAlleKontos150CHF(konten: ArrayList<IKonto>) {
    for (einKonto in konten) {
        einKonto.zahleEin(150.0)
    }
}

fun gibAlleKontoStaendeAus(konten: ArrayList<IKonto>) {

    for (einKonto in konten) {
        if (einKonto is Konto) {
            val auskunftfaehigesKonto: Konto = einKonto as Konto
            einKonto.gibKtoStandAus()
        } else {
            println("Sorry kein Konto")
        }

    }

}

