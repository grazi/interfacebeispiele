class LohnKonto(override val ktoInhaber:String,
                override val bic:Int,
                override var ktoStand:Double) : Konto(ktoInhaber, bic, ktoStand) {

    override fun zahleEin(betrag: Double) {
        super.zahleEin(betrag)
        ktoStand = ktoStand - 100
    }

}