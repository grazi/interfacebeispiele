abstract class Konto(
        open val ktoInhaber:String = "Anonym",
        open val bic:Int,
        open var ktoStand:Double = 0.0) : IKonto {


    override fun zahleEin(betrag:Double) {

        if (betrag > 0) {
            ktoStand += betrag
        }
    }

    open fun gibKtoStandAus() {
        println("$ktoStand")
    }
}